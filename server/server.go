package main

import (
	"fmt"
	g "groupie"
	"net/http"
)

func main() {

	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/", g.MainHandler)
	http.HandleFunc("/geolocation", g.LocationHandler)

	fmt.Println("Listenning at http://localhost:8080")
	err := http.ListenAndServe("localhost:8080", nil)

	if err != nil {
		fmt.Println(err)
	}

	/*
		a := g.APIRequestLoc()
		fmt.Println(a)

		b := g.APIRequestDates()
		fmt.Println(b)
	*/

}
