package groupie

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type MainStruct struct {
	Query    string
	Concerts []Concert
}

type Concert struct {
	Artist    Artist
	Locations Locations
}

type Artist struct {
	Id           int      `json:"id"`
	Image        string   `json:"image"`
	Name         string   `json:"name"`
	Members      []string `json:"members"`
	CreationDate int      `json:"creationDate"`
	FirstAlbum   string   `json:"firstAlbum"`
}

var ArtistsTab []Artist

type LocationsStruct struct {
	Tab []Locations
}

type Locations struct {
	Id        int      `json:"id"`
	Locations []string `json:"locations"`
}

var LocationsTab []Locations

type DatesStruct struct {
	Tab []Dates
}

type Dates struct {
	Id    int      `json:"id"`
	Dates []string `json:"dates"`
}

var DatesTab []Dates

func APIRequestAllConcert() []Concert {
	APIRequestArt()
	APIRequestLoc()

	fmt.Println(DatesTab)

	var concertTab []Concert
	fmt.Println(ArtistsTab)
	fmt.Println(LocationsTab)
	for i := 0; i < len(ArtistsTab); i++ {
		concertTab[i] = Concert{
			ArtistsTab[i],
			LocationsTab[i],
		}

	}

	return concertTab

}

func APIRequestSearchConcert(query string) []Concert {
	fmt.Println("lol3")
	concerts := APIRequestAllConcert()

	mytest := func(c Concert) bool {
		return strings.Contains(strings.ToLower(c.Artist.Name), strings.ToLower(query))
	}
	return filter(concerts, mytest)
}

func APIRequestArt() {

	req, errh := http.Get("https://groupietrackers.herokuapp.com/api/artists")

	if errh != nil {
		fmt.Println(errh)
	}

	d, err := ioutil.ReadAll(req.Body)

	if err != nil {
		fmt.Println(err)
	}

	json.Unmarshal(d, &ArtistsTab)

}

func APIRequestLoc() []Locations {

	req, errh := http.Get("https://groupietrackers.herokuapp.com/api/locations")

	if errh != nil {
		fmt.Println(errh)
	}

	d, err := ioutil.ReadAll(req.Body)

	if err != nil {
		fmt.Println(err)
	}

	json.Unmarshal(d, &LocationsTab)

}

func APIRequestDates() []Dates {

	req, errh := http.Get("https://groupietrackers.herokuapp.com/api/dates")

	if errh != nil {
		fmt.Println(errh)
	}

	d, err := ioutil.ReadAll(req.Body)

	if err != nil {
		fmt.Println(err)
	}

	json.Unmarshal(d, &DatesTab)

	return DatesTab
}

func filter(ss []Concert, test func(Concert) bool) (ret []Concert) {
	for _, c := range ss {
		if test(c) {
			ret = append(ret, c)
		}
	}
	return
}
