package groupie

import (
	"html/template"
	"net/http"
)

func LocationHandler(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("static/geolocation.html"))
	query := r.URL.Query().Get("q")
	var data MainStruct
	if query == "" {
		concertTab := APIRequestAllConcert()

		data = MainStruct{
			Concerts: concertTab,
		}
	} else {
		data = MainStruct{
			Query:    query,
			Concerts: APIRequestSearchConcert(query),
		}

	}

	tmpl.Execute(w, data)
}
