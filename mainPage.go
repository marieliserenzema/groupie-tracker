package groupie

import (
	"fmt"
	"html/template"
	"net/http"
)

func MainHandler(res http.ResponseWriter, req *http.Request) {

	fmt.Println(req.Method)
	tmpl := template.Must(template.ParseFiles("static/index.html"))
	query := req.URL.Query().Get("q")
	var data MainStruct
	if query == "" {
		concertTab := APIRequestAllConcert()

		data = MainStruct{
			Concerts: concertTab,
		}
	} else {
		data = MainStruct{
			Query:    query,
			Concerts: APIRequestSearchConcert(query),
		}

	}

	tmpl.Execute(res, data)

}
